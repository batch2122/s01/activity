package com.s1.activity;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {
        String firstName;
        String lastName;
        Double firstSubject;
        Double secondSubject;
        Double thirdSubject;

        Scanner appScanner = new Scanner(System.in);
        System.out.println("First Name:");
        firstName = appScanner.nextLine();
        System.out.println("Last Name:");
        lastName = appScanner.nextLine();
        System.out.println("First Subject Grade:");
        firstSubject = appScanner.nextDouble();
        System.out.println("Second Subject Grade:");
        secondSubject = appScanner.nextDouble();
        System.out.println("Third Subject Grade:");
        thirdSubject = appScanner.nextDouble();

        String fullName = firstName + " " + lastName;
        int averageGrade = (int) ((firstSubject + secondSubject + thirdSubject)/3);
        System.out.println("Good day, " + fullName + ".");
        System.out.println("Your grade average is " + averageGrade);
    }
}
